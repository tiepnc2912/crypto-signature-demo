const { ethers, utils } = require("ethers");
require("dotenv").config();

const loadWallet = () => {
  let privateKey = process.env.PRIVATE_KEY;
  return new ethers.Wallet(privateKey);
};

const wallet = loadWallet();

const computeHash = (paramTypes, paramValues) => {
  return utils.solidityKeccak256(paramTypes, paramValues);
};

const sign = (hashBytes) => {
  return wallet.signMessage(hashBytes);
};

const { ethers, utils } = require("ethers");

// Example params:
//  {
//   "nonce": 1,
//   tokenIds: [1,2,3],
//   amounts: [10,20,30],
//   timestamp: "1664435248",  // seconds
//   address: "0x8195ACBDaD88A8166Ad400943d3A172e6E5e319E"
//  }
const encodeOrderExport = (params) => {
  // convert decimal to hex
  const encodeOrder = utils.RLP.encode([
    utils.hexlify(params.nonce),
    params.tokenIds.map((e) => utils.hexlify(e)),
    params.amounts.map((e) => utils.hexlify(e)),
    utils.hexlify(Number(params.timestamp)),
    params.address,
  ]);
  return encodeOrder;
};

const decodeOrderExport = (encodeOrder) => {
  const decodedOrder = utils.RLP.decode(encodeOrder);
  // convert hex to decimal
  return {
    nonce: Number(decodedOrder[0]),
    tokenIds: decodedOrder[1].map((e) => Number(e)),
    amounts: decodedOrder[2].map((e) => Number(e)),
    timestamp: Number(decodedOrder[3]).toString(),
    address: decodedOrder[4],
  };
};

console.log(
  "Encode order: ",
  encodeOrderExport({
    nonce: 2,
    amounts: [10, 10],
    tokenIds: [1, 2],
    timestamp: "1664436738",
    address: "0x8195ACBDaD88A8166Ad400943d3A172e6E5e319E",
  })
);

const test = async () => {
  let hash = computeHash(
    ["string", "address"],
    ["1662454635", "0x8195ACBDaD88A8166Ad400943d3A172e6E5e319E"]
  );
  let hashBytes = utils.arrayify(hash);
  let signature = await sign(hashBytes);
  console.log("signature: ", signature);
  verifySignature(hashBytes, signature);
};

const verifySignature = (hashBytes, signature) => {
  try {
    let recoveredAddress = utils.verifyMessage(hashBytes, signature);
    console.log(recoveredAddress);
    return recoveredAddress === process.env.ADDRESS;
  } catch (e) {
    console.log(e);
    return false;
  }
};
test();
